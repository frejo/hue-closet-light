# About
A simple python script to turn a Philips HUE light in a closet on and off when the door opens and closes.

# Hardware setup
The closet door works like a switch. Pieces of aluminium foil is put on the door while on the door frame, two screws are connected to a Raspberry Pi. When the door is closed, it creates a connection between ground and a GPIO on the Pi. 

# Requirements
python3, pigpio, [phue (pip)](https://github.com/studioimaginaire/phue)

## Install
```
sudo apt-get update
sudo apt-get install pigpio python-pigpio python3 python3-pip
pip3 install phue
```

## Run
Make sure pigpiod is running before starting the python script.
```
sudo pigpiod
python3 closetLight.py
```

## Configuration

RightDoor, LeftDoor - Which GPIO pins are they connected to?

ClosetLight - The name of the light blulb in the closet
SurroundingLight - The name of the light surrounding the closet, so the closet light matches that brightness
TurnOnTime - Time for light to fade when turning on. The unit is ds (1s = 10ds)
TurnOffTime - Time for light to fade when turning off. The unit is ds (1s = 10ds)
BrightnessAtDark - When the surrounding light is off, how bright should the closet light be? (0-255)
OmitTime - Noise happens and callbacks happens sometimes randomly. How much time between callbacks before it should do something? The unit is in ms (1s = 1000ms)
LogicType - When should the light turn on? AND: When both doors are open. OR: When one or both of the doors are open. XOR: When ONLY one door is open. LEFT: When the left door is open. RIGHT: When the right door is open.
