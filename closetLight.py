# HUE Closet Light
# Copyright (C) 2019  Tobias Frejo

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pigpio
import time
import datetime
import phue
import urllib.request, json 
import configparser
import os

# Default variables in config file
LPIN = 27 # GREEN
RPIN = 22 # WHITE

logicType = "AND" # AND, OR, LEFT, RIGHT

turnOnTime = 50
turnOffTime = 20

brightnessWhileDark = 64

omitTime = 10 * 1000

light = 'Closet light'
surroundingLight = 'Bed lamp'

verbosity = 2 # 0: None, 1: Warning, 2: Info, 3: Debug
verbosityStrings = ['None', 'Warnings', 'Info', 'Debug']

# Other variables
path = os.path.dirname(os.path.realpath(__file__))
configFilename = 'config.ini'
configFullPath = os.path.join(path, configFilename)
config = configparser.ConfigParser()

bridge_ip = None

OPEN = True
CLOSED = False
STATE = ["CLOSED", "OPEN"]

pi = pigpio.pi()
pins = [RPIN, LPIN]

HUE = None

count = 0

lastTick = 0
countOmitted = 0

def setInputPins():
    global pins
    for pin in pins:
        pi.set_mode(pin, pigpio.INPUT)
        pi.set_pull_up_down(pin, pigpio.PUD_UP)

def createDefConfig():
    global config
    global RPIN, LPIN, logicType, turnOnTime, turnOffTime, brightnessWhileDark, omitTime, light, surroundingLight, verbosity
    log("Creating config file")
    config['DEFAULT'] = {'RightDoor': str(RPIN),
                         'LeftDoor' : str(LPIN),
                         'ClosetLight': str(light),
                         'SurroundingLight': str(surroundingLight),
                         'TurnOnTime': str(turnOnTime),
                         'TurnOffTime': str(turnOffTime),
                         'BrightnessAtDark': str(brightnessWhileDark),
                         'OmitTime': str(int(omitTime/1000)),
                         'LogicType': str(logicType),
                         'Verbosity': str(verbosity)
                        }
    with open(configFullPath, 'w') as configfile:
        config.write(configfile)
    getConfig(fromConfCreate=True)

def getConfig(firstStart=False, file=configFullPath, fromConfCreate=False):
    global config
    global RPIN, LPIN, logicType, turnOnTime, turnOffTime, brightnessWhileDark, omitTime, light, surroundingLight, verbosity
    if config.read(file):
        conf = config['DEFAULT']
        LPIN = int(conf.get('LeftDoor', LPIN))
        RPIN = int(conf.get('RightDoor', RPIN))
        logicType = conf.get('LogicType', logicType)
        turnOnTime = int(conf.get('TurnOnTime', turnOnTime))
        turnOffTime = int(conf.get('TurnOffTime', turnOffTime))
        brightnessWhileDark = int(conf.get('BrightnessAtDark', brightnessWhileDark))
        omitTime = int(conf.get('OmitTime', omitTime)) * 1000
        light = conf.get('ClosetLight', light)
        surroundingLight = conf.get('SurroundingLight', surroundingLight)
        verbosity = int(conf.get('Verbosity', verbosity))
        log("Reading config")
        log("Verbosity level: %s, %s" % (verbosity, verbosityStrings[verbosity]))
        setInputPins()
        if firstStart == False:
            updateLight()
    elif fromConfCreate == False:
        try:
            createDefConfig()
        except:
            log("Error creating config file", loglevel=1)
    else:
        log("Couldn't read config after config creation.", loglevel=1)

def getNUPNP():
    global bridge_ip
    hue_discovery_url = 'https://discovery.meethue.com/'
    try:
        req = urllib.request.urlopen(hue_discovery_url)
        data = json.loads(req.read().decode())
        bridge_ip = data[0]['internalipaddress']
        log("Retrieved HUE bridge IP: %s" % (bridge_ip))
    except:
        log("Error getting n-UPnP", loglevel=1)

def testConnection(testIp):
    descriptionUrl = ("http://%s/description.xml" % (testIp))
    req = urllib.request.Request(descriptionUrl)
    try:
        urllib.request.urlopen(req)
        return True
    except urllib.error.HTTPError as e:
        log("Error connecting to bridge with IP %s. Error code %s" % (testIp, e.code), loglevel=1)
        return False

def updateLight():
    global count
    global logicType
    leftState = pi.read( LPIN )
    rightState = pi.read( RPIN )
    lightState = HUE.get_light(light, 'on')
    turnOn = False
    if logicType == "AND":
        # Turn on if both doors are open
        if leftState and rightState:
            turnOn = True
    elif logicType == "OR":
        # Turn on if either door is open
        if leftState or rightState:
            turnOn = True
    elif logicType == "XOR":
        # Turn on if only one door is open
        if leftState != rightState:
            turnOn = True
    elif logicType == "LEFT":
        # Turn on if left door is open
        if leftState:
            turnOn = True
    elif logicType == "RIGHT":
        # Turn on if right door is open
        if rightState:
            turnOn = True
    else:
        # Wrong configuration
        log("Wrong configuration. Available logic types are 'AND', 'OR', 'XOR', 'LEFT', and 'RIGHT'.", loglevel=1)
        log("Stopping script", loglevel=0)
        exit()
    log( "%s - Left door is %s, Right door is %s." % (count, STATE[leftState], STATE[rightState]), newline=False )
    log( " Using %s logic." % (logicType), loglevel=3, newline=False, date=False )
    if turnOn != lightState:
        if turnOn:
            brightness = getSurroundingBrightness()
            log(" Setting brightness to %s." % (brightness), date=False)
            HUE.set_light(light, command(turnOnTime, True, brightness))
        else:
            log(" Turning off.", date=False)
            HUE.set_light(light, command(turnOffTime, False))
    else:
        log(" Doing nothing.", date=False)
    count += 1

def getSurroundingBrightness():
    if HUE.get_light(surroundingLight, 'on'):
        brightness = HUE.get_light(surroundingLight, 'bri')
    else:
        brightness = brightnessWhileDark
    return brightness

def command(transitiontime, on, bri=-1):
    if bri >= 0:
        return {'transitiontime': transitiontime, 'on': on, 'bri': bri}
    else:
        return {'transitiontime': transitiontime, 'on': on}

def log( logMessage, loglevel = 2, date = True, newline = True ):
    global verbosity
    if verbosity >= loglevel:
        if date:
            now = datetime.datetime.now()
            now_formatted = now.strftime("%Y-%m-%d %H:%M:%S")
            logMessage = ("[%s] %s" % (now_formatted, logMessage))
        logFile = open('light.log', 'a')
        if newline:
            print(logMessage)
            logFile.write(logMessage + '\n')
        else:
            print(logMessage, end = '')
            logFile.write(logMessage)
        logFile.close()

def handleCallback(gpio, level, tick):
    global lastTick
    global countOmitted
    if tick - lastTick > omitTime:
        log("Callback on pin %s, level %s, at tick %s. %s omitted" % (gpio, level, tick, countOmitted), loglevel=3)
        countOmitted = 0
        if gpio == LPIN or gpio == RPIN:
            updateLight()
    else:
        countOmitted += 1
    lastTick = tick

def loop():
    time.sleep(5)

def registerToBridge(firstrun=True):
    global bridge_ip
    global HUE
    if testConnection(bridge_ip):
        if firstrun:
            log("IP is valid")
        try:
            HUE = phue.Bridge(bridge_ip)
            log("Registered at bridge")
        except phue.PhueRegistrationException as e:
            log("Could not register: " + str(e) + " Retrying in 30 sec.", loglevel=1)
        if HUE == None: # Sleep outside except to minimize stacktrace with keyboardInterrupt during sleep
            time.sleep(30)
            registerToBridge(firstrun=False)
    else: 
        log("IP not valid. Stopping script", loglevel=0)
        exit()

if __name__ == '__main__':
    log("Script starting", loglevel=0)
    getConfig(firstStart=True)
    getNUPNP()
    registerToBridge()
    updateLight()
    cbL = pi.callback(LPIN, pigpio.EITHER_EDGE, handleCallback)
    cbR = pi.callback(RPIN, pigpio.EITHER_EDGE, handleCallback)
    configEditTime = os.path.getmtime(configFullPath)
    configUpdate = configEditTime
    while True:
        configEditTime = os.path.getmtime(configFullPath)
        if configUpdate != configEditTime:
            log("Config file changed. Reloading.")
            getConfig()
            configUpdate = configEditTime
        try:
            loop()
        except (KeyboardInterrupt, SystemExit):
            log("Script stopped", loglevel=0)
            exit()
